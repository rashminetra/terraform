# AWS Certificate Manager (ACM)

## Overview

This module will be responsible for creating ACM key for goaudits application in the AWS cloud.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 3.0 |

## Modules
-----------
| Name | Source | Version |
|------|--------|---------|
| <a name="acm"></a> [acm](https://registry.terraform.io/modules/terraform-aws-modules/acm/aws/latest) | terraform-aws-modules/acm/aws | ~> 3.4.0 |.
## Resources
No resource.


## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_region"></a> [region](#input\_region) | AWS region | `string` | `-` | yes |
| <a name="input_project"></a> [project](#input\_project) | Project or organisation name | `string` | `goaudits` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Application environment name (dev/prod/qa) | `string` | `-` | yes |
| <a name="input_costcentre"></a> [costcentre](#input\_costcentre) | tag for cost | `string` | `cost` | no |
| <a name="input_department"></a> [department](#input\_department) | Project department name | `string` | `operations` | no |
| <a name="input_owner"></a> [owner](#input\_owner) | Project owner name | `string` | `goaudits` | no |
| <a name="input_deletion_window_in_days"></a> [deletion_window_in_days](#input\_deletio\_window\_in\_days) | Number of days to wait before deleting KMS key | `number` | `7` | no |


## Outputs
| Name | Description |
|------|-------------|
| <a name="kms_arn"></a> [goaudits\_kms\_arn](#output\_kms\_arn) | KMS Key ARN |

## Development

### Prerequisites

- [terraform](https://learn.hashicorp.com/terraform/getting-started/install#installing-terraform)
- [terraform-docs](https://github.com/segmentio/terraform-docs)

## Authors

This project is authored by below people

- Naeem









